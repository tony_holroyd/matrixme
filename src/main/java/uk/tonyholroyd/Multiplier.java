package uk.tonyholroyd;

/**
 * Created by Tony on 30/12/2017.
 */
public class Multiplier {
    private int multiplier;

    public Multiplier(int multiplier) {
        this.multiplier = multiplier;
    }

    public int multiplyBy(int value) {
        return multiplier * value;
    }

    public float multiplyBy(float value) {
        return multiplier * value;
    }
}
