package uk.tonyholroyd;

/**
 * Created by Tony on 30/12/2017.
 */
public class Environment {
    enum Browser {unknown, edge, chrome, ie, firefox};
    public static Browser getBrowser() {
        Browser result;
        try {
            result = Browser.valueOf(System.getProperty("browser"));
        } catch (IllegalArgumentException | NullPointerException e) {
            result = Browser.unknown;
        }
        return result;
    }
}
