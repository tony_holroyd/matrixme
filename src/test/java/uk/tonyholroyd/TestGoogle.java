package uk.tonyholroyd;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.SkipException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Tony on 30/12/2017.
 */
public class TestGoogle {
    WebDriver driver;

    @BeforeClass
    public void setUp() {
        Environment.Browser browser = Environment.getBrowser();

        switch(browser) {
            case chrome:
                driver = new ChromeDriver();
                break;
            case edge:
                driver = new EdgeDriver();
                break;
            case ie:
                driver = new InternetExplorerDriver();
                break;
            case firefox:
                driver = new FirefoxDriver();
                break;
                /*
            case ie:
                String remoteHost = System.getProperty("grid.host", "localhost");
                String remotePort = System.getProperty("grid.port", "5555");
                String remoteUrl = System.getProperty("grid.url", "http://" + remoteHost + ":" + remotePort );
                try {
                    driver = new RemoteWebDriver(new URL(remoteUrl), DesiredCapabilities.internetExplorer());
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                break;
            case firefox:
                remoteHost = System.getProperty("grid.host", "localhost");
                remotePort = System.getProperty("grid.port", "4444");
                remoteUrl = System.getProperty("grid.url", "http://" + remoteHost + ":" + remotePort );
                try {
                    driver = new RemoteWebDriver(new URL(remoteUrl), DesiredCapabilities.firefox());
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                break;
                */
            default:
                throw new SkipException("This browser isn't available");
        }
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }

    @AfterMethod
    public void snapshot(java.lang.reflect.Method testMethod) throws IOException {
        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
// Now you can do whatever you need to do with it, for example copy somewhere
        File dir = new File("./target/surefire-reports/junitreports/" + this.getClass().getCanonicalName());
        dir.mkdirs();
        File destFile = File.createTempFile(testMethod.getName() + "-",".png", dir.getCanonicalFile());
        System.err.println("[[ATTACHMENT|" + destFile.getPath() + "]]");
        FileUtils.copyFile(scrFile, destFile);
        System.err.println("[[ATTACHMENT|" + destFile.getPath() + "]]");
    }

    @Test
    public void showScreen() throws InterruptedException {
        driver.get("https://www.google.co.uk/");
        WebElement element = driver.findElement(By.name("q"));
        element.sendKeys(System.getProperty("environment"));
        element.sendKeys(Keys.ENTER);
//        element = driver.findElement(By.name("btnK"));
//        element.click();
        System.out.println("hello");
        System.err.println("goodbye");
    }

    @Test
    public void detectBrowser() {
        driver.get("https://www.whatsmybrowser.org/");
    }
}
