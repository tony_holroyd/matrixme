package uk.tonyholroyd;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

/**
 * Created by Tony on 30/12/2017.
 */
public class TestMultiplier {

    private Multiplier multiplier;

    @BeforeClass
    public void setup() {
        multiplier = new Multiplier(15);
    }

    @Test
    public void testInteger() {
        int result = multiplier.multiplyBy(12);

        assertThat(result, equalTo(180));
    }

    @Test
    public void testFloat() {
        float result = multiplier.multiplyBy(1f/3);

        assertThat(result, equalTo(5f));
    }

}
