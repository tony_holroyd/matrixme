package uk.tonyholroyd;

import org.testng.annotations.Test;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
/**
 * Created by Tony on 30/12/2017.
 */
public class TestEnvironment {

    @Test
    public void testAny() {
        Environment.Browser type = Environment.getBrowser();
        assertThat(type.name(), equalTo(System.getProperty("browser")));
    }


}
